window.onload = function() {
  fetch('/courses')
    .then(response => response.text())
    .then(data => showCourses(data));
};

function addCourse() {
  var data = getFormData();
 

  fetch("/course", {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "Content-type": "application/json;charset-UTF-8" }
  })
    .then(response1 => {
      if (response1.ok) {
        fetch("/course/" + cid)
          .then(response2 => response2.text())
          .then(data => showCourse(data));
      } else {
        throw new Error(response1.statusText);
      }
    })
    .catch(e => {
      alert(e);
    });

  resetForm();
}

function showCourse(data) {
  const course = JSON.parse(data);
  newRow(course);
}

function resetForm() {
  document.getElementById("cid").value = "";
  document.getElementById("cname").value = "";
}

function showCourses(data) {
  const courses = JSON.parse(data);
  courses.forEach(course => {
    newRow(course);
  });
}

function newRow(course) {
  var table = document.getElementById("myTable");
  var row = table.insertRow(table.length);
  var td = [];

  for (var i = 0; i < table.rows[0].cells.length; i++) {
    td[i] = row.insertCell(i);
  }

  td[0].innerHTML = course.cid;
  // td[1].innerHTML = course.coursename;
    td[1].innerHTML = course.cname;

  td[2].innerHTML =
    '<input type="button" onclick="deleteCourse(this)" value="delete" id="button-1">';
  td[3].innerHTML =
    '<input type="button" onclick="updateCourse(this)" value="edit" id="button-2">';
}

var selectedRow=null
function updateCourse(r){
  selectedRow= r.parentElement.parentElement;
  //fill in the form fields with selected row data
  document.getElementById("cid").value = selectedRow.cells[0].innerHTML
  document.getElementById("cname").value = selectedRow.cells[1].innerHTML
 
  var btn = document.getElementById("button-add")
  cid = selectedRow.cells[0].innerHTML;
  if (btn){
    btn.innerHTML = "Update";
    btn.setAttribute("onclick", "update(cid)");
  }
}


//Helper function
function getFormData() {
  var formData = {
    cid: parseInt(document.getElementById("cid").value),
    cname: document.getElementById("cname").value
  };
  return formData;
}


function update(cid){
var newData = getFormData()
fetch('/course/'+cid, {
  method: "PUT",
  body: JSON.stringify(newData),
  headers: {"Content-type": "application/json; charset=UTF-8"}
  }).then (res => {
    if (res.ok) {
      selectedRow.cells[0].innerHTML = newData.stdid;
      selectedRow.cells[1].innerHTML = newData.cname;

      var button = document.getElementById("button-add");
      button.innerHTML = "Add";
      button.setAttribute("onclick", "addStudent()");
      selectedRow = null;
      resetForm();
    }else {
      alert("Server: Update request error.")
      }
  })

}

function deleteCourse(r) {
  if (confirm("Are you sure you want to DELETE this?")) {
    var selectedRow = r.parentElement.parentElement;
    var cid = selectedRow.cells[0].innerHTML;

    fetch("/course/" + cid, {
      method: "DELETE",
      headers: { "Content-type": "application/json;charset-UTF-8" }
    });

    var rowIndex = selectedRow.rowIndex;
    if (rowIndex > 0) {
      document.getElementById("myTable").deleteRow(rowIndex);
    }
  }
}
// add enroll
function addEnroll() {
  var _data = {
    stdid : parseInt(document.getElementById("sid").value),
    cid : document.getElementById("cid").vallue,
    date: ""
  }

  var sid = _data.stdid;
  var cid = _data.cid;

  if (isNaN(sid) || cid == "") {
    alert("Select valid data")
    return
  }
  fetch('/enroll', {
    method: "POST",
    body: JSON.stringify(_data),
    headers: {"COntent-type": "application/json; charset=UTF-8"}
  });
}

