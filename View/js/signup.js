// function signUp() {
//     var data = {
//       firstname: document.getElementById("fname").value,
//       lastname: document.getElementById("lname").value,
//       email: document.getElementById("email").value,
//       password: document.getElementById("pw1").value,
//       pw: document.getElementById("pw2").value,
//     };
  
//     if (data.password !== data.pw) {
//       alert("Passwords do not match!");
//       return;
//     }
  
//     fetch('/signup', {
//       method: "POST",
//       body: JSON.stringify(data),
//       headers: { "Content-Type": "application/json; charset=UTF-8" }
//     })
//       .then(response => {
//         if (response.ok) {
//           window.open("index.html", "_self");
//         } else {
//           throw new Error("Error: " + response.statusText);
//         }
//       })
//       .catch(error => {
//         alert("Error: " + error);
//       });
//   }
  
function signUp() {
  var data = {
    firstname: document.getElementById("fname").value,
    lastname: document.getElementById("lname").value,
    email: document.getElementById("email").value,
    password: document.getElementById("pw1").value,
    pw: document.getElementById("pw2").value,
  };

  if (data.password !== data.pw) {
    alert("Passwords do not match!");
    return;
  }

  fetch('/signup', {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "Content-Type": "application/json; charset=UTF-8" }
  })
    .then(response => {
      if (response.ok) {
        window.open("index.html", "_self");
      } else {
        throw new Error("Error: " + response.statusText);
      }
    })
    .catch(error => {
      alert("Error: " + error);
    });
}

//2nd attemp
// function signUp(){
//     var _data ={
//         firstname: document.getElementById("fname").value,
//         lastname: document.getElementById("lname").value,
//         email: document.getElementById("email").value,
//         password: document.getElementById("pw1").value,
//         pw: document.getElementById("pw2").value,
//     }
//     // //check whether paaword matches the confirm password

//     if (_data.password !==_data.pw){
//         alert("PASSWORD doesn't match!")
//         return
//     }
//     //post because adding data
//     fetch('/signup', {
//         method: "POST",
//         body: JSON.stringify(_data),
//         headers: {"Content-type" : " application.json; charset=UTF-8"}
//     })
//     .then(response => {
//         if (response.status == 201){  //201 is success created
//             window.open("index.html", "_self")
//       }
//     });
//   }

//3rd attempt
// function signUp(){
//     var _data ={
//         firstname: document.getElementById("fname").value,
//         lastname: document.getElementById("lname").value,
//         email: document.getElementById("email").value,
//         password: document.getElementById("pw1").value,
//         pw: document.getElementById("pw2").value,
//     }
//     // //check whether paaword matches the confirm password

//     if (_data.password !==_data.pw){
//         alert("PASSWORD doesn't match!")
//         return
//     }
//     //post because adding data
//     fetch('/signup', {
//         method: "POST",
//         body: JSON.stringify(_data),
//         headers: {"Content-type" : " application.json; charset=UTF-8"}
//     })
//     .then(response => {
//         if (response.status == 201){  //201 is success created
//             window.open("index.html", "_self")
//         }
//     });
//   }