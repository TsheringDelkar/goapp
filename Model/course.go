package Model

import "goapp/dataStore/postgres"

type Course struct {
	CourseID   int64  `json:"cid"`
	CourseName string `json:"cname"`
}

const (
	queryInsertCourse = "INSERT INTO course(courseid, coursename) VALUES($1, $2)"
	queryGetCourse    = "SELECT courseid, coursename FROM course WHERE courseid = $1"
	queryUpdateCourse = "UPDATE course SET courseid = $1, coursename = $2 WHERE courseid = $3 RETURNING courseid"
	queryDeleteCourse = "DELETE FROM course WHERE courseid = $1"
)

func (c *Course) Create() error {
	_, err := postgres.Db.Exec(queryInsertCourse, c.CourseID, c.CourseName)
	return err
}

func (c *Course) Read() error {
	return postgres.Db.QueryRow(queryGetCourse, c.CourseID).Scan(&c.CourseID, &c.CourseName)
}

func (c *Course) Update(oldID int64) error {
	err := postgres.Db.QueryRow(queryUpdateCourse, c.CourseID, c.CourseName, oldID).Scan(&c.CourseID)
	return err
}

func (c *Course) Delete() error {
	_, err := postgres.Db.Exec(queryDeleteCourse, c.CourseID)
	return err
}

//getallcourses

func GetAllCourse() ([]Course, error) {
	rows, err := postgres.Db.Query("SELECT * FROM course;")
	if err != nil {
		return nil, err
	}

	courses := []Course{}
	for rows.Next() {
		var c Course
		dbErr := rows.Scan(&c.CourseID, &c.CourseName)
		if dbErr != nil {
			return nil, dbErr
		}
		courses = append(courses, c)
	}

	rows.Close()
	return courses, nil
}
