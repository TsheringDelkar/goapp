package Controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"goapp/Model"
	"goapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func Homehandler(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("hello World"))
	if err != nil {
		fmt.Println("error: ", err)
	}
}

func Coursehandler(w http.ResponseWriter, r *http.Request) {
	p := mux.Vars(r)
	course := p["course"]
	_, err := w.Write([]byte("hello World. This course is  " + course))
	// _ = err
	if err != nil {
		fmt.Println("error: ", err)
	}
}

func Studenthandler(w http.ResponseWriter, r *http.Request) {

	p := mux.Vars(r)
	student := p["student"]
	_, err := w.Write([]byte("hello World. This student is " + student))
	_ = err
}
func Addstudent(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "add student handler")
	var stud Model.Student

	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&stud); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body") //response in json format, reused- httpResp
		return
		// response, _ := json.Marshal(map[string]string{"error": "invalid body"})
		// w.Header().Set("Content-Type", "application/json")
		// w.WriteHeader(http.StatusBadRequest)
		// w.Write(response)
		// return
	}
	defer r.Body.Close()

	saveErr := stud.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusCreated, saveErr.Error())
		return
		// response, _ := json.Marshal(map[string]string{"error": saveErr.Error()})
		// w.Header().Set("Content-Type", "application/json")
		// w.WriteHeader(http.StatusBadRequest)
		// w.Write(response)
		// return
	}
	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "student added"})
	// response, _ := json.Marshal(map[string]string{"status": "Student added"})
	// w.Header().Set("Content-Type", "application/json")
	// w.WriteHeader(http.StatusCreated)
	// w.Write(response)

}
func GetStud(w http.ResponseWriter, r *http.Request) {
	sid := mux.Vars(r)["sid"]
	stdId, idErr := getUserId(sid)

	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}

	s := Model.Student{StdId: stdId}
	getErr := s.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Student not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, s)
}

func getUserId(userIdParam string) (int64, error) {
	userId, userErr := strconv.ParseInt(userIdParam, 10, 64)
	if userErr != nil {
		return 0, userErr
	}
	return userId, nil
}

func UpdateStud(w http.ResponseWriter, r *http.Request) {
	old_sid := mux.Vars(r)["sid"]
	old_stdId, idErr := getUserId(old_sid)

	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}

	var stud Model.Student
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&stud); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()

	err := stud.Update(old_stdId)

	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, stud)

}
func DeleteStud(w http.ResponseWriter, r *http.Request) {
	sid := mux.Vars(r)["sid"]
	stdId, idErr := getUserId(sid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}

	s := Model.Student{StdId: stdId}
	if err := s.Delete(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"Status": "deleted"})

}
func GetAllStud(w http.ResponseWriter, r *http.Request) {
	students, getErr := Model.GetAllStudents()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, students)
}

// Marshal - converts objects to json  UnMarshal - converts json(byte data) to objects(struct/maps)
