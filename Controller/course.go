package Controller

import (
	"database/sql"
	"encoding/json"
	"goapp/Model"
	"goapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// add
// func AddCourse(w http.ResponseWriter, r *http.Request) {
// 	var Course Model.Course
// 	decoder := json.NewDecoder(r.Body)
// 	if err := decoder.Decode(&Course); err != nil {
// 		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json data")
// 		return
// 	}

// 	if err := Course.Create(); err != nil {
// 		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
// 		return
// 	}

//		httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"message": "Course data added"})
//	}
func AddCourse(w http.ResponseWriter, r *http.Request) {
	var Course Model.Course
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&Course); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json data")
		return
	}

	if Course.CourseName == "" {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Course name cannot be empty")
		return
	}
	if Course.CourseID == 0 {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Course ID cannot be empty")
		return
	}

	if err := Course.Create(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"message": "Course data added"})
}

// read
func GetCourse(w http.ResponseWriter, r *http.Request) {
	cid := mux.Vars(r)["cid"]
	courseID, err := getCourseID(cid)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	c := Model.Course{CourseID: courseID}
	if err := c.Read(); err != nil {
		switch err {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Course not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	httpResp.RespondWithJSON(w, http.StatusOK, c)
}

func getCourseID(courseIDParam string) (int64, error) {
	courseID, err := strconv.ParseInt(courseIDParam, 10, 64)
	if err != nil {
		return 0, err
	}
	return courseID, nil
}

// update
func UpdateCourse(w http.ResponseWriter, r *http.Request) {
	oldCID := mux.Vars(r)["cid"]
	oldCourseID, err := getCourseID(oldCID)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	var cors Model.Course
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&cors); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()

	if err := cors.Update(oldCourseID); err != nil {
		switch err {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "course not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	httpResp.RespondWithJSON(w, http.StatusOK, cors)
}

//delete

func DeleteCourse(w http.ResponseWriter, r *http.Request) {
	cid := mux.Vars(r)["cid"]
	courseID, err := getCourseID(cid)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	c := Model.Course{CourseID: courseID}
	if err := c.Delete(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})
}

func GetCourseid(courseIDParam string) (int64, error) {
	courseID, err := strconv.ParseInt(courseIDParam, 10, 64)
	if err != nil {
		return 0, err
	}
	return courseID, nil
}

// getallcourses
func GetAllCourses(w http.ResponseWriter, r *http.Request) {
	courses, getErr := Model.GetAllCourse()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, courses)
}

//
