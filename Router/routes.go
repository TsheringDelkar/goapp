package Router

import (
	"goapp/Controller"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func InitializeRoutes() {
	router := mux.NewRouter()

	router.HandleFunc("/home", Controller.Homehandler)
	router.HandleFunc("/home/{course}", Controller.Coursehandler)   //course - parseUrl - curlybrackets
	router.HandleFunc("/home/{student}", Controller.Studenthandler) //url

	router.HandleFunc("/student", Controller.Addstudent).Methods("POST")
	router.HandleFunc("/student/{sid}", Controller.GetStud).Methods("GET")
	router.HandleFunc("/student/{sid}", Controller.UpdateStud).Methods("PUT")
	router.HandleFunc("/student/{sid}", Controller.DeleteStud).Methods("DELETE")
	router.HandleFunc("/students", Controller.GetAllStud)

	//Admin
	router.HandleFunc("/signup", Controller.SignUp).Methods("POST")
	router.HandleFunc("/login", Controller.Login).Methods("POST")

	// Course routes
	//add
	router.HandleFunc("/course", Controller.AddCourse).Methods("POST")
	//Read
	router.HandleFunc("/course/{cid}", Controller.GetCourse).Methods("GET")
	//Update
	router.HandleFunc("/course/{cid}", Controller.UpdateCourse).Methods("PUT")
	//Delete
	router.HandleFunc("/course/{cid}", Controller.DeleteCourse).Methods("DELETE")
	//get all course
	router.HandleFunc("/courses", Controller.GetAllCourses)

	router.HandleFunc("/enroll", Controller.Enroll).Methods("POST")
	router.HandleFunc("/enroll/{sid}/{cid}", Controller.GetEnroll).Methods("GET")
	router.HandleFunc("/enrolls", Controller.GetEnrolls).Methods("GET")
	router.HandleFunc("/enroll/{sid}/{cid}", Controller.DeleteEnroll).Methods("DELETE")

	router.HandleFunc("/logout", Controller.Logout)

	fhandler := http.FileServer(http.Dir("./View")) //converting folder to directory
	router.PathPrefix("/").Handler(fhandler)

	log.Println("Application running on port 8080....")
	log.Fatal(http.ListenAndServe(":8080", router)) //in browser

	err := http.ListenAndServe(":8080", router)
	if err != nil {
		return
	}
}
